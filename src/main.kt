package org.westeros


sealed class Human() : Comparable<Human>

data class Andal(val surname:String? = null,val name:String? = null,val house:String? = null,val bastard:Boolean? = false) : Human(){
    override fun compareTo(other: Human) = when(house) {
        null -> 0
        "Lannister","Stark","Baratheon" -> -1
        else -> 1
    }
}
data class Dornish(val surname:String? = null,val name:String? = null,val house:String? = null,val nickname:String? = null) : Human(){
    override fun compareTo(other: Human) = -1
}
data class Dothraki(val surname:String,val name:String):Human(){
    override fun compareTo(other: Human) = 0
}
data class Valyrian(val surname:String? = null,val name:String? = null, val nickname: String?=null):Human(){
    override fun compareTo(other: Human) = -1
}
data class Ironborn(val surname:String? = null,val name:String? = null):Human(){
    override fun compareTo(other: Human) = -1
}
data class Freefolk(val surname:String? = null):Human(){
    override fun compareTo(other: Human) = 0
}
object NotAHuman:Human(){
    override fun compareTo(other: Human) = 1
}

fun main(args: Array<String>) {
    val people = listOf(
            Andal(bastard = true,surname = "John",name="Snow", house="Stark"),
            Andal("Gendry",house="Baratheon",bastard = true),
            Andal(), // Arya in disguise
            Ironborn("Balon","Greyjoy"),
            Freefolk("Ygritte"),
            Dothraki("Kahl","Drogo"),
            Dornish(surname="Oberyn",name="Martell",nickname= "Red Viper of Dorne"),
            Valyrian("Daenerys","Targaryen", nickname = "the First of Her Name, The Unburnt, Queen of the Andals," +
                    " the Rhoynar and the First Men, Queen of Meereen," +
                    " Khaleesi of the Great Grass Sea, Protector of the Realm," +
                    " Lady Regnant of the Seven Kingdoms, Breaker of Chains and Mother of Dragons"),
            NotAHuman
    )
    people.greet()
}

fun List<Human>.greet(){
    this.sorted().map { greet(it) }
}

fun greet(person:Human) = when(person){
    is Andal ->
        when(person.surname){
            null -> println("Welcome servant of The Many-Faced God")
            else -> when(person.house){
                null -> println("Welcome ${person?.surname} ${person?.name ?:""}")
                else -> println("Welcome ${person?.surname} ${person?.name ?:""} of house ${person.house}")
            }
        }
    is Ironborn -> println("Let ${person.surname} ${person.name } your servant be born again from the sea, as you were. Bless him with salt, bless him with stone, bless him with steel.")
    is Freefolk -> println("Welcome ${person.surname}")
    is Dothraki -> println("Athchomar Chomakea ${person.surname} ${person.name}")
    is Dornish -> println("Greetings ${person.surname} ${person.name}, ${person.nickname}")
    is Valyrian -> println("Greetings ${person.surname} ${person.name}, ${person.nickname}")
    is NotAHuman -> println("HOLD THE DOOR! HOLD THE DOOR! HOOODOOOR! HOOODOOOOR!")
}